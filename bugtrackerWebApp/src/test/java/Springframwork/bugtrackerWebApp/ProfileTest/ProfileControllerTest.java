package Springframwork.bugtrackerWebApp.ProfileTest;

import Springframwork.bugtrackerWebApp.Profile.Profile;
import Springframwork.bugtrackerWebApp.Profile.ProfileController;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class ProfileControllerTest {

    private static final Integer id = 1;
    private static final String firstname = "hans";
    private static final String lastname = "müller";
    private static final String email = "h.müller@web.de";
    private static final String password = "12345";



    @InjectMocks
    ProfileController profileController;

    @Mock
    private JdbcTemplate jdbcTemplate;

    @Mock
    JSONObject jsonObject;

    @Test
    void TestNewProfileFailed() throws JSONException {

        String expected = "{\"firstname: \":\"hans\",\"email\":\"h.müller@web.de\",\"authentication\":\"true\"}";
        String actual = profileController.newProfile(firstname,lastname,email, password);

        assertEquals(expected, actual);
    }

    @Test
    void TestLogIn()
    {
        String expected = "{\"authentication\":\"false\"}";
        String actual = profileController.LogIn(email, password);

        assertEquals(expected, actual);
    }

    @Test
    void TestGetAllProfiles()
    {
        List<Profile> listProfile = new ArrayList<Profile>();
        List<Profile> actual = profileController.getAllProfiles();

        assertEquals (listProfile,  actual);
    }


    @Test
    void TestDeleteProject() throws JSONException {
        profileController.deleteProject("1");

    }







}
package Springframwork.bugtrackerWebApp.ProfileTest;

import Springframwork.bugtrackerWebApp.Profile.Profile;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProfileTest {


    private Profile profile;

    @Test
    public void constructorTest(){
        profile = new Profile(0, "hans", "müller", "h.müller@web.de", "passwort1");
        assert(profile.getId() == 0);
        assert(profile.getFirstname().equals("hans"));
        assert(profile.getLastname().equals("müller"));
        assert(profile.getEmail().equals("h.müller@web.de"));
        assert(profile.getPassword().equals("passwort1"));
    }

    @Test
    public void constructor2Test(){
        profile = new Profile("hans", "müller", "h.müller@web.de", "passwort1");
        assert(profile.getFirstname().equals("hans"));
        assert(profile.getLastname().equals("müller"));
        assert(profile.getEmail().equals("h.müller@web.de"));
        assert(profile.getPassword().equals("passwort1"));
    }

    @Test
    void TestSetter()
    {
        Profile profile = new Profile(0, "hans", "müller", "h.müller@web.de", "passwort1");
        profile.setId(1);
        profile.setFirstname("test");
        profile.setLastname("test");
        profile.setEmail("test");
        profile.setPassword("test");

    }
    @Test
    void mapRow() {
    }
}
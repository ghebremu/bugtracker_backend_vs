package Springframwork.bugtrackerWebApp.BugTest;

import Springframwork.bugtrackerWebApp.Bug.Bug;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BugTest {


    @Test
    void TestConstructor1()
    {
        Bug bug = new Bug("test", "test", "test", "test", "test", 0);
        assert (bug.getTitle().equals("test"));
        assert (bug.getDifficulty().equals("test"));
        assert (bug.getStatus().equals("test"));
        assert (bug.getWirtten_in().equals("test"));
        assert (bug.getProject_id()==0);
    }

    @Test
    void TestConstructor2()
    {
        Bug bug = new Bug(0, "test", "test", "test", "test", "test", 0);
        assert (bug.getId() == 0);
        assert (bug.getTitle().equals("test"));
        assert (bug.getDifficulty().equals("test"));
        assert (bug.getStatus().equals("test"));
        assert (bug.getWirtten_in().equals("test"));
        assert (bug.getProject_id()==0);
    }

    @Test
    void TestSetters()
    {
        Bug bug = new Bug(0, "test", "test", "test", "test", "test", 0);
        bug.setId(1);
        bug.setTitle("test");
        bug.setDifficulty("test");
        bug.setStatus("test");
        bug.setWirtten_in("test");
        bug.setDate("test");
        bug.setProject_id(1);
    }
}
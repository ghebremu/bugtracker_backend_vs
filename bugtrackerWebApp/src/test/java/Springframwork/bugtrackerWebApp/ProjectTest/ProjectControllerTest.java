package Springframwork.bugtrackerWebApp.ProjectTest;


import Springframwork.bugtrackerWebApp.Project.Project;
import Springframwork.bugtrackerWebApp.Project.ProjectController;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ProjectControllerTest {

    @InjectMocks
    ProjectController projectController;


    @Test
    void TestProjectController()
    {
        projectController.testProjectController();
    }

    @Test
    void TestcreateProject() throws JSONException {
        String id = "0";
        Project project_info = new Project();

        projectController.createProject(id, project_info);

    }

    @Test
    void TestdeleteProject() throws JSONException {
        String id = "0";
        String title = "test";

        projectController.deleteProject(id, title);
    }

    @Test
    void TestRenameProject() throws JSONException {
        String id = "0";
        String new_title = "test";
        String old_title = "test";

        projectController.renameProject(id, old_title, new_title);
    }
}

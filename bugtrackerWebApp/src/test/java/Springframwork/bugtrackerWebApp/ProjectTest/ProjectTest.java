package Springframwork.bugtrackerWebApp.ProjectTest;

import Springframwork.bugtrackerWebApp.Project.Project;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProjectTest {

    @Test
    void TestConstructor()
    {
        Project project = new Project(0,"test", "test", "test", "test");
        assert (project.getUserId() == 0);
        assert (project.getTitle().equals("test"));
        assert (project.getProject_description().equals("test"));
        assert (project.getMembers().equals("test"));
        assert (project.getDate().equals("test"));
    }
    @Test
    void TestConstructor2()
    {
        Project project = new Project("test", "test", "test", "test");
        assert (project.getTitle().equals("test"));
        assert (project.getProject_description().equals("test"));
        assert (project.getMembers().equals("test"));
        assert (project.getDate().equals("test"));
    }
    
    @Test
    void TestSetters()
    {
        Project project = new Project(0,"test", "test", "test", "test");
        project.setUserId(1);
        project.setTitle("test");
        project.setProject_description("test");
        project.setMembers("test");
        project.setDate("test");
    }

}
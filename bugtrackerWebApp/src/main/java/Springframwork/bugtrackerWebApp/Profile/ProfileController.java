package Springframwork.bugtrackerWebApp.Profile;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

// alow cors origon on the whole controller
// controller for members
@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = {"http://localhost:8090", "http://127.0.0.1:5500"})

public class ProfileController {

  @Autowired
  private JdbcTemplate jdbcTemplate;

  // create new Profile
  @PostMapping(value = "/signUp")
  public String newProfile(
          @RequestParam(value = "firstname") String firstname,
          @RequestParam(value = "lastname") String lastname,
          @RequestParam(value = "email") String email,
          @RequestParam(value = "password") String password

  ) throws JSONException {
    boolean bool = false;
    JSONObject response = new JSONObject();
  try{
         String sql = "INSERT INTO profiles(firstname, lastname, email,password)" +
                 "VALUES("+ "'" + firstname + "'" + "," + "'" + lastname +"'" + "," + "'"+ email + "'"+ "," + "'" + password + "'" +");";
         jdbcTemplate.update(sql);

         response.put("authentication", Boolean.toString(true));
         response.put("firstname: ", firstname);
         response.put("email", email);
    }catch (Exception e){

    System.out.println("error: " + e.toString());
    response.put("authentication", Boolean.toString(false));


  }
  System.out.println(response.toString());
  return response.toString();

  }





  @PostMapping(value = "/login")
  public String LogIn(
          @RequestParam(value = "email") String email,
          @RequestParam(value = "password") String password
  ) {
    // sql command for get password
    String sql_getEmail = "SELECT email FROM profiles WHERE email = ?";
    String sql_getPassword = "SELECT password FROM profiles WHERE email = ?";
    String sql_getId = "SELECT id FROM profiles WHERE email = ?";
    String sql_getFirstname = "SELECT firstname FROM profiles WHERE email = ?";

    try {
      // get email from db
      String email_db = (String) jdbcTemplate.queryForObject(sql_getEmail, new Object[]{email},String.class);

      // get password from db
      String password_db = (String) jdbcTemplate.queryForObject(sql_getPassword, new Object[]{email},String.class);

      // get firstname form db
      String firstname_db = (String) jdbcTemplate.queryForObject(sql_getFirstname, new Object[]{email},String.class);

      // compare password from db with typed password from user
      boolean bool = password.equals(password_db);

      // create json object for response
      JSONObject response = new JSONObject();

      if (bool){
        // get id
        String id_db = (String) jdbcTemplate.queryForObject(sql_getId, new Object[]{email},String.class);

        // set authentication
        response.put("authentication" , Boolean.toString(bool));
        // set id
        response.put("user_id", id_db);

        // send firstname
        response.put("email", email );

        response.put("firstname", firstname_db);

      }else {
        response.put("authentication" , Boolean.toString(bool));
      }


      return  response.toString();


    }catch (Exception e){

      return "error:" + e.toString();
    }

  }

  // get all profiles
  @GetMapping(value = "/getAllProfiles")
  public List<Profile> getAllProfiles(){
    String sql = "SELECT * FROM profiles";

    List<Profile> p = jdbcTemplate.query(
            sql,
            new Profile());

    return p;


  }

  // get onne Profile
  @PostMapping(value = "/getProfile")
  public Profile getProfile(@RequestParam(value = "id")int id ) {

    String sql = "SELECT * FROM profiles WHERE ID = ?";

    Profile p = new Profile();
    p = jdbcTemplate.queryForObject(sql, new Object[]{id}, new Profile());
    p = jdbcTemplate.queryForObject(sql, new Object[]{id}, new Profile());
    System.out.println("object: " + p.toString());

    return p;
  }


  //Delete a Profile
  @DeleteMapping(value = "/deleteProfiles")
  public String deleteProject(@RequestParam (value = "user_id") String user_id) throws JSONException
  {
    System.out.println("User_id = "+ user_id);
    //JSONObject
    JSONObject response = new JSONObject();

    boolean bool = false;

    try
    {
      String sql = "DELETE FROM profiles WHERE id =" + user_id;
      jdbcTemplate.update(sql);
      bool = true;
    }
    catch (Exception e)
    {
      System.out.println("error: " + e);
      response.put("deleteProfiles", bool);
    }

    return response.put("deleteProfiles", bool).toString();
  }





}
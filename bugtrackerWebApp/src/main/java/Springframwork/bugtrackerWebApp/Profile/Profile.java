
package Springframwork.bugtrackerWebApp.Profile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Profile implements RowMapper<Profile> {

  private Integer id;
  private String firstname;
  private String lastname;
  private String email;
  private String password;

  public Profile(
    Integer id,
    String firstname,
    String lastname,
    String email,
    String password
  )
  {
    this.id = id;
    this.firstname = firstname;
    this.lastname = lastname;
    this.email = email;
    this.password = password;
  }

  public Profile(
    String firstname,
    String lastname,
    String email,
    String password
  ) {
    this.firstname = firstname;
    this.lastname = lastname;
    this.email = email;
    this.password = password;
  }

  public Profile(){};

    @Autowired
    private JdbcTemplate jdbcTemplate;


  @Override
  public String toString() {
    return (
      "Members{" +
      "id=" +
      id +
      ", firstname='" +
      firstname +
      '\'' +
      ", lastname='" +
      lastname +
      '\'' +
      ", email='" +
      email +
      '\'' +
      ", password='" +
      password +
      '\'' +
      '}'
    );
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
        this.id = id;
    }

  public String getFirstname() {
    return firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public String getEmail() {
    return email;
  }

  public String getPassword() {
    return password;
  }

  @Override
  public Profile mapRow(ResultSet resultSet, int i) throws SQLException {
    Profile profile = new Profile();
    profile.setId(resultSet.getInt("id"));
    profile.setFirstname(resultSet.getString("firstname"));
    profile.setLastname(resultSet.getString("lastname"));
    profile.setEmail(resultSet.getString("email"));
    profile.setPassword(resultSet.getString("password"));
    return profile;
  }


}




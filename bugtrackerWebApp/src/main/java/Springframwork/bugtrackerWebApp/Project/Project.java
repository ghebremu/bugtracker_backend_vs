package Springframwork.bugtrackerWebApp.Project;


import org.springframework.jdbc.core.RowMapper;

import javax.swing.tree.TreePath;
import java.time.LocalDate;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Project implements RowMapper<Project> {
    private int user_id;
    private String title;
    private String project_description;
    private String members;
    private String date;


    public Project( String title, String project_description, String members, String date) {
       // this.user_id = user_id;
        this.title = title;
        this.project_description = project_description;
        this.members = members;
        this.date = date;
    }

    public Project( int user_id, String title, String project_description, String members, String date) {
         this.user_id = user_id;
        this.title = title;
        this.project_description = project_description;
        this.members = members;
        this.date = date;
    }

    public Project(){};

    @Override
    public String toString() {
        return "Project{" +
                "title='" + title + '\'' +
                ", project_description='" + project_description + '\'' +
                ", members='" + members + '\'' +
                ", date=" + date +
                '}';
    }



   /* public void setUser_id(int user_id) {
        this.user_id = user_id;
    }*/

    public void setTitle(String title) {
        this.title = title;
    }

    public void setProject_description(String project_description) {
        this.project_description = project_description;
    }

    public void setMembers(String members) {
        this.members = members;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setUserId(int user_id) {
        this.user_id = user_id;
    }







    public String getTitle() {
        return title;
    }

    public String getProject_description() {
        return project_description;
    }

    public String getMembers() {
        return members;
    }

    public String getDate() {
        return date;
    }

    public int getUserId() {
        return user_id;
    }



    @Override
    public Project mapRow(ResultSet resultSet, int i) throws SQLException {

        Project project = new Project();
        project.setTitle(resultSet.getString("title"));
        project.setProject_description(resultSet.getString("project_description"));
        project.setMembers(resultSet.getString("members"));
        project.setDate(resultSet.getString("date"));
        project.setUserId(resultSet.getInt("user_id"));

        return project;
    }
}

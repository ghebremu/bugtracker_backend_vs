package Springframwork.bugtrackerWebApp.Project;


import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// alow cors origon on the whole controller
@CrossOrigin(origins = "http://localhost:8090")
@RestController
@RequestMapping(value = "/user")
public class ProjectController {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    @GetMapping
    public String testProjectController() {

        return "<h1>porject controller endpoint called </h1>";
    }


    // get all project porjects from id
    @GetMapping(path = "/getProjects/{user_id}")
    public List<Project> getProjects(@PathVariable String user_id)  {
        String sql = "SELECT * FROM projects WHERE user_id = " + user_id;


        List<Project> projectList = jdbcTemplate.query( sql, new Project());

        return projectList;

    }

    // create new Project
    @PostMapping(value = "createProject")
    public String createProject(@RequestParam(value = "id") String id, @RequestBody Project project_info) throws JSONException {
        System.out.println("userid: " + id);

        System.out.println("project inforamtion: " + project_info.toString());

        // create json object for response
        JSONObject response = new JSONObject();
        boolean bool = false;


        try {
            String sql = "INSERT INTO projects( title, project_description, members, date, user_id) VALUES ( "  + "'" + project_info.getTitle() + "'" + "," + "'" + project_info.getProject_description() + "'" + "," + "'" + project_info.getMembers() + "'" + "," + "'" + project_info.getDate() + "'" +  "," + "'" + id + "'" + ")";
            jdbcTemplate.update(sql);
            bool = true;
        } catch (Exception e) {
            System.out.println("error: " + e);
            response.put("createdProject", bool);
        }
        // update table
        return response.put("createdProject", bool).toString();
    }


    //delete Project
    @DeleteMapping(value = "deleteProject")
    public String deleteProject(@RequestParam(value = "user_id") String user_id  ,@RequestParam (value= "project_title") String projecct_title) throws JSONException {
        System.out.println("userid: " + user_id);

        System.out.println("project title: " + projecct_title);

        // create json object for response
        JSONObject response = new JSONObject();
        boolean bool = false;

        try {
            String sql = "DELETE FROM projects WHERE user_id =" +user_id +"AND title= "+ "'"+projecct_title+ "'";
            jdbcTemplate.update(sql);
            bool = true;
        } catch (Exception e) {

            System.out.println("error: " + e);
            response.put("deleteProject", bool);
        }
        return response.put("deleteProject", bool).toString();
    }

    @PutMapping(value = "/renameProject")
    public String renameProject(@RequestParam (value = "user_id") String user_id, @RequestParam (value = "project_title") String old_title, @RequestParam (value = "new_title") String new_title) throws JSONException
    {
        System.out.println("userid: " + user_id);

        System.out.println("project title: " + old_title);

        System.out.println("new title: " + new_title);

        JSONObject respone = new JSONObject();
        boolean bool = false;

        try
        {
            String sql = "UPDATE projects SET title="+"'"+ new_title +"'"+" WHERE user_id ="+"'"+ user_id +"'"+" and title="+"'"+ old_title +"'";
            jdbcTemplate.update(sql);
            bool = true;
        }
        catch (Exception e)
        {
            bool = false;
            respone.put("renameProject", bool);
        }

        return respone.put("renameProject", bool).toString();



    }




}

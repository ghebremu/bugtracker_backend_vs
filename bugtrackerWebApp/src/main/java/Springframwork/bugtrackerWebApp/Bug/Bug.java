package Springframwork.bugtrackerWebApp.Bug;

import org.springframework.jdbc.core.RowMapper;

import javax.swing.tree.TreePath;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Bug implements RowMapper<Bug> {
    private int id;
    private String title;
    private String difficulty;
    private String status;
    private String wirtten_in;
    private String date;
    private int project_id;


    public Bug(String title, String difficulty, String status, String wirtten_in, String date, int project_id) {
        this.title = title;
        this.difficulty = difficulty;
        this.status = status;
        this.wirtten_in = wirtten_in;
        this.date = date;
        this.project_id = project_id;
    }


    public Bug(int id, String title, String difficulty, String status, String wirtten_in, String date, int project_id) {
        this.id = id;
        this.title = title;
        this.difficulty = difficulty;
        this.status = status;
        this.wirtten_in = wirtten_in;
        this.date = date;
        this.project_id = project_id;
    }

    public Bug(){};





    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWirtten_in() {
        return wirtten_in;
    }

    public void setWirtten_in(String wirtten_in) {
        this.wirtten_in = wirtten_in;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getProject_id() {
        return project_id;
    }

    public void setProject_id(int project_id) {
        this.project_id = project_id;
    }


    // access to database

    @Override
    public Bug mapRow(ResultSet resultSet, int i) throws SQLException {
        Bug bug = new Bug();

        return bug;
    }




}

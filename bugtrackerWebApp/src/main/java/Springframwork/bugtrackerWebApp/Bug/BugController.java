package Springframwork.bugtrackerWebApp.Bug;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// alow cors origon on the whole controller
@CrossOrigin(origins = "http://localhost:8090")
// controller for members
@RestController
@RequestMapping(value = "/project")
public class BugController {
}
